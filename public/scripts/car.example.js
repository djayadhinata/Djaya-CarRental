class Car {
  static list = [];

  static init(cars) {
    this.list = cars.map((i) => new this(i));
  }

  constructor({
    id,
    plate,
    manufacture,
    model,
    image,
    rentPerDay,
    capacity,
    description,
    transmission,
    available,
    type,
    year,
    options,
    specs,
    availableAt,
  }) {
    this.id = id;
    this.plate = plate;
    this.manufacture = manufacture;
    this.model = model;
    this.image = image;
    this.rentPerDay = rentPerDay;
    this.capacity = capacity;
    this.description = description;
    this.transmission = transmission;
    this.available = available;
    this.type = type;
    this.year = year;
    this.options = options;
    this.specs = specs;
    this.availableAt = availableAt;
  }

  render() {
    return `

    
      
        
          <div class="card"style="min-height:100%;" >
            <div class="card-body ">
              <img src="${this.image}" class="img-fluid" alt="" style="height:400px; width:100%;object-fit:cover;"/>
              <h6 class="card-subtitle mt-2">${this.model} ${this.type}</h6>
              <h5 class="card-subtitle mb-2"><b>${this.rentPerDay}</b></h5>
              <p class="card-text">
                ${this.description}
              </p>
              <p><img src="img/fi_users.png" class="me-2" alt="" /> ${this.capacity}</p>
              <p><img src="img/fi_settings.png" class="me-2" alt="" /> ${this.transmission}</p>
              <p><img src="img/fi_calendar.png" class="me-2" alt="" /> ${this.year}</p>
              <a class="btn btn-success mx-auto" href="#" role="button">Sewa</a>
            </div>
          </div>
       
      
    
    `;
  }
}
// <p>id: <b>${this.id}</b></p>
// <p>plate: <b>${this.plate}</b></p>
// <p>manufacture: <b>${this.manufacture}</b></p>
// <p>model: <b>${this.model}</b></p>
// <p>available at: <b>${this.availableAt}</b></p>
// <img src="${this.image}" alt="${this.manufacture}" width="64px">
