class App {
  constructor() {
    this.clearButton = document.getElementById("clear-btn");
    this.loadButton = document.getElementById("load-btn");
    this.searchButton = document.getElementById("search-btn");
    this.tanggalSewa = document.getElementById("tanggal-sewa");
    this.jamSewa = document.getElementById("jam-sewa");
    this.jumlahPenumpang = document.getElementById("jumlah-penumpang");
    this.carContainerElement = document.getElementById("cars-container");
  }

  async init() {
    await this.load();

    // Register click listener
    this.clearButton.onclick = this.clear;
    this.loadButton.onclick = this.run;
    this.searchButton.onclick = this.run;
  }

  run = async () => {
    await this.load();
    this.clear();
    Car.list.forEach((car) => {
      const node = document.createElement("div");
      node.className = "col-4";
      node.innerHTML = car.render();
      this.carContainerElement.appendChild(node);
    });
  };

  async load() {
    const tanggal = this.tanggalSewa.value;
    const jam = this.jamSewa.value;
    const penumpang = this.jumlahPenumpang.value;

    console.log("tanggal", tanggal);
    console.log("jam", jam);
    console.log("penumpang", penumpang);

    const tanggalJam = new Date(`${tanggal} ${jam}`);
    console.log("tangalJam", tanggalJam);

    const epochTime = tanggalJam.getTime();
    console.log("epochTime", epochTime);

    const cars = await Binar.listCars((item) => {
      const filterByCapacity = item.capacity >= penumpang;
      const filterByTanggalSewa = item.availableAt.getTime() < epochTime;
      return filterByCapacity && filterByTanggalSewa;
    });
    Car.init(cars);
  }

  clear = () => {
    let child = this.carContainerElement.firstElementChild;

    while (child) {
      child.remove();
      child = this.carContainerElement.firstElementChild;
    }
  };
}
