console.log("Implement servermu disini yak 😝!");

const express = require("express");
const app = express();
const port = 3000;

//statis file
app.use(express.static("public"));
app.use("/css", express.static(__dirname + "public/css"));
app.use("/images", express.static(__dirname + "public/images"));
app.use("/img", express.static(__dirname + "public/img"));
app.use("/scripts", express.static(__dirname + "public/scripts"));

app.get("", (req, res) => {
  res.sendFile(__dirname + "public/index.html");
});

// List on port 3000
app.listen(port, () => console.info(`listening on port ${port}...`));
